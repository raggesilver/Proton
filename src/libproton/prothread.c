#include "prothread.h"
#include <pthread.h>
#include <stdio.h>

struct _Prothread {
    GObject parent_instance;

    GThread        *_thread;
    void         *(*_function)(void *);
    void           *res;
    gpointer       *args;
    const gchar    *name;

};

G_DEFINE_TYPE (Prothread, prothread, G_TYPE_OBJECT)

enum {
    COMPLETED,
    LAST_SIGNAL
};

static guint signals [LAST_SIGNAL];

static void
prothread_class_init (ProthreadClass *klass) {

    signals [COMPLETED] =
        g_signal_new("completed",
                     G_TYPE_FROM_CLASS(klass),
                     G_SIGNAL_RUN_LAST,
                     0,
                     NULL, NULL, NULL,
                     G_TYPE_NONE, G_TYPE_POINTER);

}

static void
prothread_init (Prothread *self)
{
    
}

static void *
do_run (void *self)
{
    void *res = ((Prothread*)self)->_function(
                    (void*)((Prothread*)self)->args);

    ((Prothread*)self)->res = res;
    g_idle_add((GSourceFunc)emit_completed, (gpointer)self);
    printf("Thread finished\n");

    return NULL;
}

gboolean
emit_completed (void *self)
{
    g_signal_emit((Prothread*)self, COMPLETED, 0, ((Prothread*)self)->res);
    return TRUE;
}

void
run (Prothread *self,
     const      gchar *name,
     void      *_function,
     gpointer  *args)
{

    self->name = name;
    self->_function = _function;
    self->args = args;

    self->_thread = g_thread_new(self->name,
                                 do_run,
                                 (void*)self);
    
    printf("Thread ran\n");
}
