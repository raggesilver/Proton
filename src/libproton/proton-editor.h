#ifndef PROTON_EDITOR
#define PROTON_EDITOR

#include <glib-object.h>
#include <gtk/gtk.h>
#include <gtksourceview/gtksource.h>
#include "proton-file.h"

G_BEGIN_DECLS

#define PROTONEDITOR_TYPE protoneditor_get_type ()

G_DECLARE_FINAL_TYPE (Protoneditor, protoneditor, PROTONEDITOR, PROTONEDITOR, GObject)

Protoneditor * protoneditor_new ();
Protoneditor * protoneditor_new_from_pfile (Pfile *);

G_END_DECLS

#endif