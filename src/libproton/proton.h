#ifndef PROTON_H
#define PROTON_H

#include <gtk/gtk.h>
#include <proton-notebook.h>

typedef struct _Proton {
    ProtonNotebook          *notebook;
    GtkApplicationWindow    *window;
} Proton;

void init_proton (int argc, char *argv[]);

#endif