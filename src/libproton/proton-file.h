#ifndef PFILE_H
#define PFILE_H

#include <glib-object.h>

typedef struct _Pfile {
    GObject parent_instance;

    char       *content;
    char       *path;
    gboolean    modified;
} Pfile;

gboolean    pfile_load_content      (Pfile *);//, GError **);
gboolean    pfile_save_content      (Pfile *self, GError **err);
Pfile      *pfile_new               ();

#endif