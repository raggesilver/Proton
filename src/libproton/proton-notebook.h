#ifndef PROTON_NOTEBOOK_H
#define PROTON_NOTEBOOK_H

#include <gtk/gtk.h>
#include <proton-editor.h>
#include <proton-file.h>

typedef struct _ProtonNotebookTab {
    void  *next, *prev;
    
    GtkWidget           *layout;
    Pfile               *pfile;
    Protoneditor        *editor;
} ProtonNotebookTab;

typedef struct _ProtonNotebook {
    GtkWidget           *layout;
    ProtonNotebookTab   *active_tab;
    ProtonNotebookTab   *first_tab;
} ProtonNotebook;

#endif