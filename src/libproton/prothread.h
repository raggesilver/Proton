#ifndef PROTHREAD
#define PROTHREAD

#include <glib-object.h>

G_BEGIN_DECLS

#define PROTHREAD_TYPE prothread_get_type ()

G_DECLARE_FINAL_TYPE (Prothread, prothread, PROTHREAD, PROTHREAD, GObject)

void run (Prothread *self, const gchar *name, void *_function, gpointer *args);
gboolean emit_completed (void *self);

G_END_DECLS

#endif