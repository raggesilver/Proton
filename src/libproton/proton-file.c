#include "proton-file.h"
#include <stdio.h>

#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/io.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>

#define CHUNK_SIZE 4096

// G_DEFINE_TYPE (Pfile, pfile, G_TYPE_OBJECT)
// G_DEFINE_TYPE (Pfile, pfile, G_TYPE_OBJECT)

enum {
    PFILE_ERROR,
    PFILE_ERROR_READ,
    PFILE_ERROR_WRITE,
    PFILE_ERROR_STRETCH
};

Pfile *
pfile_new() {
    Pfile *self = (Pfile *)malloc(sizeof(Pfile));
    return self;
}

/**
 * Use mmap to read the contents of a file and store them in Pfile->content.
 * This function may take a while, so it should be called in a thread
 * @param self  *Pfile
 * @param err   **GError
 * @return gboolean if the operation was sucessful or not
 */
gboolean
pfile_load_content (Pfile *self)//,
                    // GError **err)
{
    if (!self->path) {
        return FALSE;
    }

    // int f = open(self->path, O_RDONLY);
    // if (!f) {
    //     // g_set_error(err,
    //     //             PFILE_ERROR,
    //     //             PFILE_ERROR_READ,
    //     //             "Could not read file");
    //     return FALSE;
    // }

    // struct stat _status;
    // fstat(f, &_status);

    // size_t total_size = _status.st_size;

    // self->content = mmap(0, total_size, PROT_READ, MAP_PRIVATE, f, 0);
    // if (!self->content) {
    //     close(f);
    //     return FALSE;
    // }

    // close(f);

    // WORKS
    clock_t begin = clock();

    FILE *f = fopen(self->path, "r");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  //same as rewind(f);

    self->content = malloc(fsize + 1);
    fread(self->content, fsize, 1, f);
    self->content[fsize] = 0;

    fclose(f);

    /* here, do your time-consuming job */

    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

    printf("Time: %f\n", time_spent);

    return TRUE;
}

/**
 * Use mmap to save the content of a Pfile
 * @param self  *Pfile
 * @return gboolean whether the operation succeded or not
 */
gboolean
pfile_save_content (Pfile *self,
              GError **err)
{
    if (!self->path || !self->content) {
        return FALSE;
    }

    int f = open(self->path, O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
    if (!f) {
        g_set_error(err,
                    PFILE_ERROR,
                    PFILE_ERROR_WRITE,
                    "Could not write/create file");
        return FALSE;
    }

    size_t text_size = strlen(self->content) + 1; // The '\0' character

    // Stretch file size
    if(lseek(f, text_size - 1, SEEK_SET) == -1) {
        close(f);
        g_set_error(err,
                    PFILE_ERROR,
                    PFILE_ERROR_STRETCH,
                    "Could not write/create file");
        close(f);
        return FALSE;
    }

    /* This makes the file so that it may have its new streched size */
    if(write(f, "", 1) == -1) {
        close(f);
        g_set_error(err,
                    PFILE_ERROR,
                    PFILE_ERROR_WRITE,
                    "Could not write file");
        return FALSE;
    }

    int *map = mmap(0, text_size - 1,
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED, f, 0);
    if (map == MAP_FAILED) {
        close(f);
        g_set_error(err,
                    PFILE_ERROR,
                    PFILE_ERROR_WRITE,
                    "Could not write file");
        return FALSE;
    }

    memcpy(map, self->content, text_size - 1);

    close(f);

    return TRUE;
}
