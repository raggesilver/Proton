#include "proton-plugin-manager.h"


ProtonPluginManager *
proton_plugin_manager_new ()
{
    ProtonPluginManager *self = (ProtonPluginManager *)malloc(sizeof(ProtonPluginManager));

    // This might take longer but at least we don't leave a huge buffer containing /home/a lol
    self->cwd = (char *)malloc(FILENAME_MAX * sizeof(char));
    getcwd(self->cwd, FILENAME_MAX);

    self->cwd = realloc(self->cwd, (strlen(self->cwd)) * sizeof(char));

    // FIXME this is just stupid I think
    self->required_files = malloc(3 * sizeof(char *));
    self->required_files[0] = "plugin.c";
    self->required_files[1] = "plugin.h";
    self->required_files[2] = "plugin.json";

    return self;
}


void
proton_plugin_manager_load_plugins (ProtonPluginManager *self)
{
    const char *path = (const char *)g_build_path("/", g_get_home_dir (), ".proton", "plugins", NULL);

    struct stat sb;

    if (!(stat(path, &sb) == 0 && S_ISDIR(sb.st_mode))) {
        // The plugins folder doesn't exist
        return;
    }

    DIR *dir;
    struct dirent *entry;

    if (!(dir = opendir(path)))
        return;

    // Loop through everything in the plugins folder
    while ((entry = readdir(dir))) {

        struct stat entry_stat;
        stat(entry->d_name, &entry_stat);

        // Skip if the entry is not a folder
        if (!(S_ISDIR(entry_stat.st_mode)))
            continue;
        
        // Skip if the folder is . or ..
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue;

        printf("Plugin found: %s\n", entry->d_name);
        // printf("Required files count: %zu\n", sizeof(self->required_files) / sizeof(self->required_files[0]));

        // TODO this should be moved to another file
        // TODO check if the necessary files are present
        // following https://stackoverflow.com/a/46141568/5609203

        void *handle = NULL;
        void (*init)();

        handle = dlopen(g_build_path("/", path, entry->d_name, "plugin.so", NULL), RTLD_LAZY);

        if (!handle) {
            printf("Error loading plugin, %s.\n", dlerror());
            continue;
        }

        // TODO there are many necessary error checking here
        init = (void (*)()) dlsym(handle, "plugin_init");
        (*init)();
    }

}

// int main (int argc,
//           char *argv[])
// {
//     

//     printf("%s\n", buff);
//     return 0;
// }