#ifndef PROTON_PLUGIN_MANAGER_H
#define PROTON_PLUGIN_MANAGER_H

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <dlfcn.h>

typedef struct _ProtonPluginManager {
    
    void    *window;
    char    *cwd;
    char   **required_files;

} ProtonPluginManager;

ProtonPluginManager *proton_plugin_manager_new ();

void proton_plugin_manager_load_plugins (ProtonPluginManager *self);

#endif
