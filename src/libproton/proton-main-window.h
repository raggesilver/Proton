#ifndef PROTON_MAIN_WINDOW_H
#define PROTON_MAIN_WINDOW_H

#include <gtk/gtk.h>
#include <proton-hash-table.h>
#include <proton-plugin-manager.h>

typedef struct _ProtonMainWindow {

    GtkWidget           *layout;
    GtkApplication      *app;
    GtkBuilder          *builder;

    GtkWidget           *header_bar;

    ProtonPluginManager *plugin_manager;

    /* The proton hash table will be used to store the plugins widgets */
    ProtonHashTable     *widgets;
} ProtonMainWindow;

ProtonMainWindow *proton_main_window_new (GtkApplication *app);

#endif
