#include "proton-notebook.h"

ProtonNotebook *
proton_notebook_new (GtkWidget *layout)
{
    ProtonNotebook *self = (ProtonNotebook *)malloc(sizeof(ProtonNotebook));
    self->layout = layout;
    return self;
}

ProtonNotebookTab *
proton_notebook_get_last_tab (ProtonNotebook *self)
{
    if(self->first_tab == NULL)
        return self->first_tab;

    ProtonNotebookTab *cur = self->first_tab;
    while (cur->next != NULL) cur = cur->next;

    return cur;
}

ProtonNotebookTab *
proton_notebook_tab_new ()
{
    ProtonNotebookTab *self = (ProtonNotebookTab *)malloc(sizeof(ProtonNotebookTab));
    return self;
}

void
proton_notebook_new_tab (ProtonNotebook *self,
                         Pfile          *pfile)
{
    ProtonNotebookTab *tab = proton_notebook_tab_new ();
    tab->next = NULL;
    tab->prev = proton_notebook_get_last_tab (self);

    tab->pfile  = pfile;
    tab->editor = protoneditor_new_from_pfile (tab->pfile);

    if(tab->prev == NULL)
        self->first_tab = tab;
}