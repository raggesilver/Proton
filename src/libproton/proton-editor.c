#include "proton-editor.h"

struct _Protoneditor {
    GObject             parent_instance;
    Pfile              *pfile;
    GtkWidget          *layout;
    GtkWidget          *sview;
    GtkSourceBuffer    *sbuff;
};

G_DEFINE_TYPE (Protoneditor, protoneditor, G_TYPE_OBJECT)

static void
protoneditor_class_init (ProtoneditorClass *klass)
{

}

static void
protoneditor_init (Protoneditor *self)
{
    self->pfile     = pfile_new ();
    self->sview     = gtk_source_view_new ();
    self->sbuff     = gtk_source_buffer_new (NULL);
    self->layout    = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);

    // gtk_text_view_set_buffer (GTK_TEXT_BUFFER (self->sbuff));
}

Protoneditor *
protoneditor_new ()
{
    Protoneditor *self = (Protoneditor*)malloc(sizeof(Protoneditor));
    return self;
}

Protoneditor *
protoneditor_new_from_pfile (Pfile *pfile)
{
    Protoneditor *self = protoneditor_new();
    self->pfile = pfile;
    return self;
}