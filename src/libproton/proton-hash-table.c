/**
 * ProtonHashTable is my implementation of an associative array in C
 */

#include "proton-hash-table.h"


ProtonHashTable *
proton_hash_table_new ()
{
    ProtonHashTable *self = (ProtonHashTable *)malloc(sizeof(ProtonHashTable));

    self->len = 0;

    return self;
}


static void
proton_hash_table_init_values (ProtonHashTable *self)
{
    if (self->values == NULL) {
        self->values = malloc(sizeof(void *));
    }

    if (self->indexes == NULL) {
        self->indexes = malloc(sizeof(char *));
    }
}


int
proton_hash_table_push (ProtonHashTable *self,
                        const char *index,
                        void *value)
{
    // If the pointers are uninitialized
    if (self->len == 0)
        proton_hash_table_init_values(self);

    // Reallocing space
    self->values = realloc(self->values, (self->len + 1) * sizeof(void *));
    self->values[self->len] = value;

    self->indexes = realloc(self->indexes, (self->len + 1) * sizeof(char *));
    self->indexes[self->len] = (char *)index;

    // Add one to the index
    self->len++;

    // TODO check for memory allocation errors and return FALSE if any

    return TRUE;
}


void *
proton_hash_table_get_by_index (ProtonHashTable *self,
                                const char *index)
{
    for (int i = 0; i < self->len; i++) {
        // If the index was found
        if (strcmp(index, self->indexes[i]) == 0) {
            return self->values[i];
        }
    }
    // If none was found return null
    return NULL;
}

// testing
// int main (int argc,
//           char *argv[])
// {
//     ProtonHashTable *table = proton_hash_table_new();
//     const char *val = "Paulo";
//     const char *index = "name";

//     const char *val2 = "Queiroz";
//     const char *index2 = "surname";

//     proton_hash_table_push(table, index, (void *)val);
//     proton_hash_table_push(table, index2, (void *)val2);

//     printf("Hello, %s %s.\n", (char *)proton_hash_table_get_by_index(table, "name"), (char *)proton_hash_table_get_by_index(table, "surname"));

//     return 0;
// }