#ifndef PROTON_HASH_TABLE_H
#define PROTON_HASH_TABLE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef FALSE
#define FALSE (0)
#endif

#ifndef TRUE
#define TRUE (!FALSE)
#endif

typedef struct _ProtonHashTable {

    void   **values;
    char   **indexes;
    int      len;

} ProtonHashTable;

/* */
ProtonHashTable *proton_hash_table_new ();

int     proton_hash_table_push         (ProtonHashTable *self,
                                        const char *index,
                                        void *value);
void   *proton_hash_table_get_by_index (ProtonHashTable *self,
                                        const char *index);

#endif
