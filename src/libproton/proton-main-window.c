#include "proton-main-window.h"

/**
 * Initialize class
 */
static void
proton_main_window_init (ProtonMainWindow *self)
{
    /* Builder */
    self->builder = gtk_builder_new_from_resource("/com/raggesilver/Proton/resources/layouts/proton-main-window.ui");

    self->layout = GTK_WIDGET (gtk_builder_get_object (self->builder, "window"));
    self->header_bar = GTK_WIDGET (gtk_builder_get_object (self->builder, "headerbar"));

    gtk_window_set_application (GTK_WINDOW (self->layout), self->app);

    /* Hash table */
    self->widgets = proton_hash_table_new ();

    /* Plugin manager */
    self->plugin_manager = proton_plugin_manager_new ();
    self->plugin_manager->window = (void *)self;
    proton_plugin_manager_load_plugins (self->plugin_manager);
}

ProtonMainWindow *
proton_main_window_new (GtkApplication *app)
{
    ProtonMainWindow *self = (ProtonMainWindow *)malloc(sizeof(ProtonMainWindow));
    self->app = app;
    proton_main_window_init(self);
    return self;
}
