#include <proton.h>
#include <proton-file.h>
#include <proton-main-window.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include <gtksourceview/gtksource.h>
#include <stdio.h>
#include <glib.h>

static void
on_open (GtkApplication *app,
		 GFile         **files,
		 gint          	 n_files,
		 gchar        	*hint,
		 gpointer      	 user_data)
{
	ProtonMainWindow *window = proton_main_window_new (app);
	gtk_window_present (GTK_WINDOW (window->layout));
}

static void
on_activate (GtkApplication *app,
			 gpointer 		 user_data)
{
	GtkWidget *window = gtk_application_window_new(app);
	gtk_window_set_title(GTK_WINDOW(window), "proton");
	gtk_window_present(GTK_WINDOW(window));
}

void init_proton(int argc,
				 char *argv[])
{
	printf("init_proton\n");

	g_autoptr(GtkApplication) app = gtk_application_new ("com.raggesilver.Proton", G_APPLICATION_HANDLES_OPEN);

	g_signal_connect(app, "activate", G_CALLBACK(on_activate), NULL);
	g_signal_connect(app, "open", G_CALLBACK(on_open), NULL);

	g_application_run (G_APPLICATION (app), argc, argv);

	// exit(0);
	return;

}