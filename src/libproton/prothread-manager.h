#ifndef PROTHREAD_MANAGER
#define PROTHREAD_MANAGER

#include <glib-object.h>

G_BEGIN_DECLS

#define PROTHREAD_TYPE_MANAGER prothread_manager_get_type ()

G_DECLARE_FINAL_TYPE (ProthreadManager, prothread_manager, PROTHREAD, MGR, GObject)

G_END_DECLS

#endif